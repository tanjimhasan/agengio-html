
    /* ==============================================


        <Table Of Content>

        01: Sticky Header
        02: Back To Top
        03: Mean Menu
        04: Check Data
        05: Background Image
        06: Owl Carousel
        07: Service Expanded
        08: Counter Up
        09: Accordion Activation
        10: Google Map
        11: Video Pop Up
        12: Validation
        13: Activate Animation
        
    ============================================== */

    (function($) {
        "use strict";
        
        let windows = $(window);
        let screenSize = windows.width();

        /*==================================
        01: Sticky Header
        ====================================*/
        
        $(window).on('scroll', function(e){
            if($(this).scrollTop() < 100){
                $('.main-header').removeClass('sticky')
            }else
                $('.main-header').addClass('sticky')        
        });

        $(window).on('load', function(){
            if($(window).scrollTop() >= 100){
                $('.main-header').addClass('sticky')
            }
        });

        /* 02: Back To Top 
        ==============================================*/

        let $backToTopBtn = $('.backToTop');

        if ($backToTopBtn.length) {
            let scrollTrigger = 300, // px
            backToTop = function () {
                let scrollTop = $(window).scrollTop();
                if (scrollTop > scrollTrigger) {
                    $backToTopBtn.addClass('show');
                } else {
                    $backToTopBtn.removeClass('show');
                }
            };

            backToTop();

            $(window).on('scroll', function () {
                backToTop();
            });

            $backToTopBtn.on('click', function (e) {
                e.preventDefault();
                $('html,body').animate({
                    scrollTop: 0
                }, 700);
            });
        }


        /* 03: Mean Menu
        ==============================================*/

        $('.main-menu ul li:has(ul)').addClass('has-submenu');

        let mainMenuNav = $('.main-menu nav');
        mainMenuNav.meanmenu({
            meanScreenWidth: '991',
            meanMenuContainer: '.mobile-menu',
            meanMenuClose: '<span class="menu-close"></span>',
            meanMenuOpen: '<span class="menu-bar"></span>',
            meanRevealPosition: 'right',
            meanMenuCloseSize: '0',
        });


        /*04: Check Data
        ====================================*/

        let checkData = function (data, value) {
            return typeof data === 'undefined' ? value : data;
        };

        /* 05: Background Image
        ==============================================*/

        let bgImg = $('[data-bg-img]');

            bgImg.css('background', function(){
                return 'url(' + $(this).data('bg-img') + ') center center';
        });


        /*06: Owl Carousel
        ====================================*/

        let $owlCarousel = $('.owl-carousel');
         
        $owlCarousel.each(function () {
            let $t = $(this);
                
            $t.owlCarousel({
                items: checkData( $t.data('owl-items'), 1 ),
                margin: checkData( $t.data('owl-margin'), 0 ),
                loop: checkData( $t.data('owl-loop'), true ),
                smartSpeed: 450,
                autoplay: checkData( $t.data('owl-autoplay'), false ),
                autoplayTimeout: checkData( $t.data('owl-speed'), 8000 ),
                center: checkData( $t.data('owl-center'), false ),
                animateOut: checkData( $t.data('owl-animate'), false ),
                autoHeight: checkData( $t.data('owl-auto-height'), false ),
                nav: checkData( $t.data('owl-nav'), false ),
                navText: ["<i class='fa fa-angle-right'></i>" , "<i class='fa fa-angle-left'></i>"],
                dots: checkData( $t.data('owl-dots'), false ),
                responsive: checkData( $t.data('owl-responsive'), {} )
            });
        });

        /*07: Service Expanded
        ====================================*/

        $(".service-list1").on("click",".title1", function () {

            $(this).next().slideDown();

            $(".service-text1").not($(this).next()).slideUp();

        });

        $(".service-list1").on("click",".title1", function () {

            $(this).parent().addClass("active").siblings().removeClass("active");

        });

        $(".service-list2").on("click",".title2", function () {
            console.log(this);
            $(this).next().slideDown();

            $(".service-text2").not($(this).next()).slideUp();

        });

        $(".service-list2").on("click",".title2", function () {

            $(this).parent().addClass("active").siblings().removeClass("active");

        });
    

        /* 08: Counter Up
        ==============================================*/
        $('.count').counterUp({
            delay: 30,
            time: 2e3
        });

        /*09: Accordion Activation
        ====================================*/

        $(".accordion-block").on("click",".accordion-title", function () {

            $(this).next().slideDown();

            $(".accordion-text").not($(this).next()).slideUp();

        });

        $(".accordion-block").on("click",".accordion-title", function () {

            $(this).parent().addClass("active").siblings().removeClass("active");

        });

        /* 10: Google Map
        ==============================================*/

        if($('#location-map').length){
            let googleMapSelector = $('#location-map');
            let myCenter = new google.maps.LatLng(23.755876, 90.380823);
            

            function initialize() {
                let mapProp = {
                    center: myCenter,
                    zoom: 16,
                    scrollwheel: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    styles: [
                        {
                            "featureType": "landscape.man_made",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#ebebeb"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape.natural",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#d0e3b4"
                                }
                            ]
                        },
                        {
                            "featureType": "landscape.natural.terrain",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "labels",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.business",
                            "elementType": "all",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.medical",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#fbd3da"
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#bde6ab"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "road",
                            "elementType": "labels",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                }
                            ]
                        },
                        {
                            "featureType": "road.local",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "black"
                                }
                            ]
                        },
                        {
                            "featureType": "transit.station.airport",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#cfb2db"
                                }
                            ]
                        },
                        {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#c7e5fd"
                                }
                            ]
                        }
                    ]
                };
                
                let map = new google.maps.Map(document.getElementById("location-map"),mapProp);
            }
            if (googleMapSelector.length) {
                google.maps.event.addDomListener(window, 'load', initialize);
            }
        }

        $('[data-toggle="tooltip"]').tooltip();

        /* 11: Video Pop Up
        ==============================================*/

        $('.video-play-btn').magnificPopup({
            type: 'iframe',
            mainClass: 'mfp-with-zoom',
            removalDelay: 300,
            zoom: {
                enabled: true,
                duration: 300,
                easing: 'ease-in-out',
            }
        });

        $('.advance-btn').on('click' , function(e){
            e.preventDefault();
            $('.advance-search-content').slideToggle();
        });


        /* 12: Validation
        ==============================================*/
        let inputs = document.querySelectorAll('.checkValidation');

        function checkValidation(){
            if(this.value == ''){
                this.parentElement.classList.add('invalid');
            }else{
                this.parentElement.classList.remove('invalid');
            }
        }

        if(inputs){
            inputs.forEach(input => input.addEventListener('input', checkValidation));
            inputs.forEach(input => input.addEventListener('focusout', checkValidation));
        }

        /* 24: Activate Animation
        ==============================================*/

        $(window).on('load', function(){

            var $animateEl = $('[data-animate]');

            $animateEl.each(function () {
                var $el = $(this),
                    $name = $el.data('animate'),
                    $duration = $el.data('duration'),
                    $delay = $el.data('delay');

                $duration = typeof $duration === 'undefined' ? '0.6' : $duration ;
                $delay = typeof $delay === 'undefined' ? '0.1' : $delay ;

                $el.waypoint(function () {
                    $el.addClass('animated ' + $name)
                    .css({
                            'animation-duration': $duration + 's',
                            'animation-delay': $delay + 's'
                    });
                }, {offset: '93%'});
            });
        });

        
    })(jQuery);