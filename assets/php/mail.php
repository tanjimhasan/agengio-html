<?php
// Naming Constant
define( "RECIPIENT_NAME", "Agengio" );

define( "RECIPIENT_EMAIL", "tanjim.hasan.pro@gmail.com" );
// Values
$success = false;
$senderName = isset( $_POST['name'] ) ? preg_replace( "/[^\.\-\' a-zA-Z0-9]/", "", $_POST['name'] ) : "";
$senderEmail = isset( $_POST['email'] ) ? preg_replace( "/[^\.\-\_\@a-zA-Z0-9]/", "", $_POST['email'] ) : "";
$message = isset( $_POST['message'] ) ? preg_replace( "/(From:|To:|BCC:|CC:|Subject:|Content-Type:)/", "",
$_POST['message'] ) : "";

$subject = "Mail From $senderName";
// Send the email
if ( $senderName && $senderEmail && $message ) {
  $recipient = RECIPIENT_NAME . " <" . RECIPIENT_EMAIL . ">";
  $headers = "From: " . $senderName . " <" . $senderEmail . ">";
  $success = mail( $recipient, $subject, $message, $headers );
  if($success){
    http_response_code(200);
    echo "Thank You! Your message has been sent.";
  }else {
    // Set a 500 (internal server error) response code.
    http_response_code(500);
    echo "Oops! Something went wrong and we couldn't send your message.";
}
  //Set Location
}else{
    echo "Please complete the form and try again.";
}
?>